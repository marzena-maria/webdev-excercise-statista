console.log('External script implemented successfully'); // Just for debugging, delete later

// initial value
const myArray = [];

// array of numbers numbers 1 to 100 
for(let value = 1; value <= 100; value++) {
    myArray.push(value);
};
console.log(myArray);

// replace values callback
function newArray (element) {
    if (element % 3 === 0 && element % 5 === 0) {
        return 'FizzBuzz'
    } 
    if (element % 3 === 0) {
        return 'Buzz'
    }
    if (element % 5 === 0) {
        return 'Fizz'
    } 
    return element;
};

// new array
const myNewArray = myArray.map(newArray);
console.log(myNewArray);

// display array as an unsorted HTML list
const unorderedList = document.createElement('ul');
unorderedList.classList.add('fizzBuzzList__list');
const listContainer = document.querySelector("#fizzBuzzList");
listContainer.appendChild(unorderedList);

myNewArray.forEach(item => {
    let listItem = document.createElement('li');
    listItem.classList.add('fizzBuzzList__list_item');
    unorderedList.appendChild(listItem);
    listItem.innerHTML += item;
});



